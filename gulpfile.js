const elixir = require('laravel-elixir');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir(mix => {
    mix.sass('app.scss', './assets/css')
        .sass('admin.scss', './assets/admin/css/app.css')
        .webpack('admin.js', './assets/admin/js/app.js')
        .webpack('app.js', './assets/js').browserSync({
        proxy: 'vmware.dev'
    });
});
