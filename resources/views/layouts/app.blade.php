<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Dare VMware @yield('page_title')</title>
    <link rel="stylesheet" href="{{ asset('/assets/css/app.css') }}">
    @yield('header')
</head>
<body class=" @yield('body_class') ">
    @yield('content')
    @yield('footer')
</body>
</html>
