@if (session()->has('flash_notification.message'))
    @if (session()->has('flash_notification.overlay'))
        @include('flash::modal', [
            'modalClass' => 'flash-modal',
            'title'      => session('flash_notification.title'),
            'body'       => session('flash_notification.message')
        ])
    @else
        <div class="callout
                    {{ session('flash_notification.level') }}"
                {{ session()->has('flash_notification.important') ? 'data-closable' : '' }}
        >
            @if(session()->has('flash_notification.important'))
                <button class="close-button" aria-label="Dismiss alert" type="button" data-close>
                    <span aria-hidden="true">&times;</span>
                </button>
            @endif

            {!! session('flash_notification.message') !!}
        </div>
    @endif
@endif
