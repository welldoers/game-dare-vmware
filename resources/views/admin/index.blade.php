@extends('layouts.admin')

@section('content')
    <div class="row">
        <div class="small-10 small-offset-1 columns">
            <br>
            <div class="row collapse">
                <div class="small-9 medium-10 columns">
                    <h4>Participants in the Facebook campaign.</h4>
                </div>
                <div class="small-3 medium-2 columns text-right">
                    <a class="button secondary" href="{{ url('logout') }}">Logout</a>
                </div>
            </div>

            @if($participants->count())
                <table class="stack hover">
                    <thead>
                    <tr>
                        <th>Date</th>
                        <th>Name</th>
                        <th>Email</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($participants as $participant)
                        <tr>
                            <td>{{ $participant->created_at->format('H:i:s d.m.Y') }}</td>
                            <td><a target="_blank" href="{{ $participant->link }}">{{ $participant->name }}</a></td>
                            <td>{{ $participant->email }}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            @else
                <div class="callout info">
                    No participants registered.
                </div>
            @endif
        </div>
    </div>
@endsection
