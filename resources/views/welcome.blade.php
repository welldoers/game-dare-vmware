@extends('layouts.app')

@section('content')
    <div class="container">
        <header>
            <img class="logo" src="{{ asset('/assets/images/vmware.svg') }}" alt="Dare VMware">
        </header>

        <section id="step-one">
            <h1>Enter the raffle in 3 simple steps:</h1>

            <h3 class="top-margin">1. Login with Facebook</h3>
            <p class="loading hide">Loading please wait...</p>
            <a id="login-button" disabled class="hide facebook-style" href="#">
                <span class="icon icon-facebook-box"></span>
                <span>Login <em>with</em> Facebook</span>
            </a>
        </section>
        <section id="step-two" class="hide">
            <h2>Two more steps to go:</h2>
            <h3 class="top-margin">2. Like VMware Bulgaria Facebook page</h3>
            <div class="fb-like" data-href="{{ config('site.facebook.pageUrl') }}" data-layout="button" data-action="like" data-size="large" data-show-faces="false" data-share="false"></div>
            <h3 class="top-margin">3. Share VMware Bulgaria Facebook page</h3>
            <div class="fb-share-button" data-href="{{ config('site.facebook.pageUrl') }}" data-layout="button" data-size="large" data-mobile-iframe="true"><a class="fb-xfbml-parse-ignore" target="_blank" href="https://www.facebook.com/sharer/sharer.php?u={{ urlencode(config('site.facebook.pageUrl')) }}&amp;src=sdkpreparse">Share</a></div>
            <p class="finish-holder text-center">
                <a href="{{ config('site.facebook.pageUrl') }}" class="finish-button">Finish</a>
            </p>
        </section>
    </div>
    <div class="preloader">
        <img src="{{ asset('/assets/images/vmware.svg') }}" alt="Dare VMware">
        <p>Loading...</p>
    </div>
@endsection

@section('body_class', 'is-loading')

@section('header')
    <script>
        window.appConfig = {
            baseUrl: '{{ url('/') }}',
            token: '{{ csrf_token() }}',
            facebook: {
                appId: '{{ config('site.facebook.app_id') }}',
                version: '{{ config('site.facebook.api_version') }}',
                pageUrl: '{{ config('site.facebook.pageUrl') }}',
            }
        };
    </script>
@endsection

@section('footer')
    <script src="{{ asset('/assets/js/app.js') }}"></script>
@endsection
