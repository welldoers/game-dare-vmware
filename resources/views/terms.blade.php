@extends('layouts.app')

@section('content')
    <div class="container">
        <header>
            <img class="logo" src="{{ asset('/assets/images/vmware.svg') }}" alt="Dare VMware">
        </header>
        <section class="main">


            <h1>VMWARE BULGARIA OCULUS RIFT RAFFLE – TERMS & CONDITIONS</h1>
            <ol>
                <li>This raffle is open to Bulgaria, Greece, Romania, Ukraine residents aged 18 and over and excludes
                    employees and their immediate families of the Promoter, its agents or anyone professionally
                    connected to the promotion.
                </li>
                <li>There is 1 x Oculus Rift VR headset to be won in total.</li>
                <li>Entries for a chance to win Oculus Rift can be made from 6-th of October, 2016 users of VMware
                    Bulgaria VR Experience application for Android and iOS who log for the raffle with their Facebook
                    account.
                </li>
                <li>The draw will take place in the first quarter of 2017. The winner will be drawn by means of an
                    independently audited random computer system and notified within 7 days of the draw with a comment
                    on the Facebook post on the VMware Bulgaria Facebook page. The winner will be required to private
                    message the page within 28 days of the announcement to confirm acceptance of the prize and provide a
                    valid postal address. Prizes will be dispatched by standard post within 30 days of notification of
                    the winner and their acceptance of the prize. In the event of the winner not responding with an
                    acceptance of the prize within a further 14 days, the Promoter reserves the right to withdraw the
                    prize entitlement and draw another winner. The Promoter reserves the right at its sole discretion to
                    disqualify any individual found to be tampering with the operation of the promotion, setting up
                    multiple accounts, using multiple identities or to be acting in any manner deemed by the Promoter to
                    be in violation of the terms and conditions; or to be acting in any manner deemed by the Promoter to
                    be disruptive.
                </li>
                <li>Bulk, trade or third parties applications are not permitted, and will be disqualified.</li>
                <li>No cash alternative will be available. Entry instructions are deemed to form part of the full terms
                    and conditions.
                </li>
                <li>The Promoter’s decision is final and binding in all matters.</li>
                <li>By entering this raffle, entrants agree to be bound by the terms and conditions governing this
                    promotion.
                </li>
                <li>This promotion is in no way sponsored, endorsed or administered by, or associated with Facebook and
                    Facebook shall not be liable in any way whatsoever to the participants. Furthermore, any questions,
                    comments or complaints regarding the promotion will be directed to the Promoter, not Facebook.
                </li>
                <li>
                    Limitations of liability: Neither the Promoter, its agents or anyone professionally connected to the
                    promotion, assume any responsibility or liability for:
                    <ol type="a">
                        <li>Any incorrect or inaccurate personal data entry, or for any faulty or failed electronic data
                            transmissions.
                        </li>
                        <li>Communications line failure, regardless of cause, with regards to any equipment, systems,
                            networks, lines, satellites, servers, computers or providers utilized in any aspect of this
                            promotion.
                        </li>
                        <li>Inaccessibility or unavailability of the internet or the Facebook page or any combination
                            thereof.
                        </li>
                        <li>Any injury or damage to entrants upon delivery and/or use of the prize.</li>
                    </ol>
                </li>
                <li>Owing to exceptional circumstances outside its reasonable control and only where circumstances make
                    this unavoidable, the Promoter reserves the right to cancel or amend the promotion or these terms
                    and conditions at any stage but will always endeavor to minimize the effect to participants in order
                    to avoid undue disappointment.
                </li>
                <li>To ensure fairness and the integrity of the raffle to all participants, the Promoter will not enter
                    into discussions regarding the running of this raffle via Facebook, but will respond to questions
                    via <a href="mailto:vmware@united-partners.com">mail</a>.
                </li>
                <li>The Promoter reserves the right to modify or expand the terms and conditions. Such changes take
                    effect after their publication.
                </li>
            </ol>

            <p>Promoter: VMwareBulgaria, 16A, “G.M. Dimitrov” Blvd., Sofia, 1797, Bulgaria</p>
        </section>
    </div>
@endsection
