@extends('layouts.admin')

@section('content')
    <div class="container">
        <div class="row">
            <div class="small-10 small-offset-1 medium-8 medium-offset-2 large-4 large-offset-4 columns">
                @include('flash::message')

                <form class="form" role="form" method="POST" action="{{ url('/login') }}">
                    {{ csrf_field() }}
                    <fieldset class="fieldset">
                        <h3>Enter your username and password.</h3>
                        <div class="{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="name" class="">Username</label>

                            <div class="">
                                <input id="name" type="text" class="" name="name" value="{{ old('name') }}" required
                                       autofocus>

                                @if ($errors->has('name'))
                                    <span class="form-error is-visible">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="">Password</label>

                            <div class="">
                                <input id="password" type="password" class="" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="form-error is-visible">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="">
                            <div class="">
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" name="remember"> Remember Me
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="">
                            <div class="">
                                <button type="submit" class="button primary">
                                    Login
                                </button>
                            </div>
                        </div>
                    </fieldset>
                </form>
            </div>
        </div>
    </div>
@endsection
