@extends('layouts.app')

@section('content')
    <div class="container">
        <header>
            <img class="logo" src="{{ asset('/assets/images/vmware.svg') }}" alt="Dare VMware">
        </header>
        <section class="main">
            <h1>Privacy Policy</h1>
            <p>This Privacy Policy governs the manner in which VMware Bulgaria collects, uses, maintains and discloses
                information collected from users (each, a "User") of the <a target="_blank"
                                                                            href="https://www.facebook.com/vmwarebg/?fref=ts">website</a>
                ("Site").</p>

            <h2>Personal identification information</h2>
            <p>We may collect personal identification information from Users in a variety of ways, including, but not
                limited to when Users visit our site and register on the site. Users may be asked for, as appropriate,
                name, surname and email address. We will collect personal identification information from Users only if
                they voluntarily submit such information to us. Users can always refuse to supply personally
                identification information, except that it may prevent them from engaging in certain Site related
                activities like participating in the raffle “Win Oculus Rift VR headset” .</p>

            <h2>How we use collected information</h2>
            <p>Facebook VMware Bulgaria may collect and use Users personal information for the following purposes:</p>
            <ul>
                <li>To run a raffle “Win Oculus Rift VR headset”</li>
                <li>To send information about the raffle and exciting job offers</li>
            </ul>
            <h2>How we protect your information</h2>
            <p>We adopt appropriate data collection, storage and processing practices and security measures to protect
                against unauthorized access, alteration,
                disclosure or destruction of your personal information, stored on our Site.</p>
            <h2>Sharing your personal information</h2>
            <p>We do not sell, trade, or rent Users personal identification information to others.</p>
            <h2>Changes to this privacy policy</h2>
            <h2>Electronic newsletters</h2>
            <p>If User decides to opt-in to our mailing list, they will receive emails that may include company job
                offers
                and career news.</p>
            <h2>Changes to this privacy policy</h2>
            <p>VMware Bulgaria has the discretion to update this privacy policy at any time. When we do, we will revise
                the updated date at the bottom of this page.
                We encourage Users to frequently check this page for any changes to stay informed about how we are
                helping to protect
                the personal information we collect. You acknowledge and agree that it is your responsibility to review
                this privacy policy periodically and become aware of modifications.</p>
            <h2>Your acceptance of these terms</h2>
            <p>By using this Site, you signify your acceptance of this policy. If you do not agree to this policy,
                please do not use our Site. Your continued use of the Site following the posting of changes to this
                policy will be deemed your acceptance of those changes. </p>

            <h2>Contacting Us</h2>
            <p>If there are any questions regarding this privacy policy, you may contact us using the information below.
                <br/>
                <a target="_blank" href="https://www.facebook.com/vmwarebg/?fref=ts">Website</a>
                <br/>
                VMware Bulgaria, 16A, “G.M. Dimitrov” Blvd., Sofia, 1797, Bulgaria
                <br/>
                Mail: <a href="mailto:vmware@united-partners.com"><strong>vmware@united-partners.com</strong></a>
                <br/>
                Last updated: <strong>29.11.2016</strong>
            </p>
        </section>
    </div>
@endsection
