<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

use App\Participant;
use Illuminate\Http\Request;

//Route::get('/', function () {
//    return 'Coming soon...';
//});

Route::get('/', function () {
    return view('welcome');
});

Route::get('/privacy-policy', function () {
    return view('privacy');
});

Route::get('/terms', function () {
    return view('terms');
});

Route::post('/participate', function(Request $request) {
   if(!$request->ajax()) {
       throw new HttpException('Method Not Allowed', 405);
   }

   if(Participant::add($request->all())) {
       return response()->json([
           'success' => true,
           'message' => 'Thank you. You\'ll be redirected to our official Facebook page.'
       ]);
   }

   return response()->json([
       'success' => false
   ]);

});

//Auth::routes();
Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('login', 'Auth\LoginController@login');
Route::get('logout', 'Auth\LoginController@logout')->name('logout');

Route::group(['prefix' => 'admin', 'middleware' => 'auth'], function () {
    Route::get('/', 'AdminController@index');
});
