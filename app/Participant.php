<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Validator;

class Participant extends Model
{
    protected $table = 'participants';
    protected $fillable = [
        'email',
        'first_name',
        'last_name',
        'link',
        'name',
        'birthday'
    ];

    public static $rules = [
        'email' => 'required|email',
        'first_name' => 'max:50',
        'last_name' => 'max:50',
        'full_name' => 'max:100',
        'link' => 'url',
        'birthday' => 'max: 20'
    ];

    public static function add($data)
    {
        $validation = Validator::make($data, self::$rules);
        if($validation->fails()) {
            return false;
        }

        $existing = self::where('email', $data['email'])->first();
        if($existing) {
            return $existing->update($data);
        }

        return self::create($data);

    }
}
